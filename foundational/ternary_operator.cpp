#include <iostream>
using namespace std;

int main() {
    // ternary working
    // cond ? exp if true : exp if false
    // int x = 130;
    // int y = 20;
    // string result = (x > y) ? "X is greater" : "Y is greater";
    // cout << result;

    int age = 20;
    bool isEligibleToVote = (age > 18) ? "1" : "0";
    cout << isEligibleToVote;
    return 0;
}