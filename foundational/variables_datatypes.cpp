#include <iostream>
using namespace std;

int main()
{
    // variables - named storage memory location
    // <data type> <variable name> = value;

    // int age = 30;
    // cout << "My age is : " << age << endl;

    // variable declaration : stores garbage values (random)
    // int age;
    // cout << age;

    // // definition
    // int age = 21;
    // cout << "current age : " << age << endl;

    // // manipulation and updation
    // age = 300;
    // cout << "new age : " << age;

    // int
    int count = 3;
    // float
    float share = 3.12;
    // char
    char alphabet = 'z';
    // double
    double weight = 445.2232453;
    // boolean : 0 - false, 1 - true
    bool isMale = true;
    bool isChild = 1;

    // cout << count << endl;
    // cout << share << endl;
    // cout << alphabet << endl;
    // cout << weight << endl;
    // cout << isMale << endl;
    // cout << bool(isChild) << endl;
    // 1 byte == 8 bits
    // cout << sizeof(char);

    // avoid declaring values in the same scope to avoid error, solution is to have create new scope and define there or have a new set
    // int age = 10;
    // {
    //     int age = 30;
    // }
    // {
    //     int age = 393;
    // }
    return 0;
}

// data type helps us understand - size of data | type of data
// c++ datatypes -> primitive | derived | user-defined
// primitive datatypes -> int, float, double, bool, void, char