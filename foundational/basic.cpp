// header files - define the exact location
#include <iostream>
// std namespace - dictionary
using namespace std;

int main()
{
    // entry point - executes and starts here
    // bracket - scoped
    cout << "Namaste Dunia" << endl;
    // status : successful #return 0, any non-zero return code means unsuccessful execution
    return 0;
}

// for cout to work we need iostream header file with namespace std ..
// << operator : insertion .. std::cout #scope-resolution
// "..." #string
// endl - new line
// ; - statement termination