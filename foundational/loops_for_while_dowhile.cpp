#include <iostream>
using namespace std;

int main() {
    // manual entries
    // cout << "sai" << endl;
    // cout << "sai" << endl;
    // cout << "sai" << endl;
    // cout << "sai" << endl;
    // cout << "sai" << endl;

    // smart way using loop
    // for(intialization; cond; update){}
    // for (int count = 0; count < 5; count++) {
    //     cout << "Sai" << endl;
    // }

    // // counting from 1 to 5
    // for (int count = 1; count <= 5; count++) {
    //     cout << (count) << endl;
    // }
    // // counting from 51 to 69
    // for (int count = 51; count <= 69; count++) {
    //     cout << (count) << endl;
    // }

    // for (int i = 1; i < 10; i++) {
    //     if (i == 5) {
    //         continue;
    //     }
    //     cout << i << " ";
    // }

    // while loop
    int i = 1;
    while (i <= 10) {
        cout << i << endl;
        i++;
    }
    return 0;
}