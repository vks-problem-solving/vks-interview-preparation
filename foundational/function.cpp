#include <iostream>
using namespace std;

int sum(int a, int b) {
    return a + b;
}

void printMyName() {
    cout << "sai" << endl;
}

int main() {
    // function - can take inputs, process them, return a result .. making it more readable and maintable
    // int result = sum(3, 2);
    // cout << "The sum is :" << result << endl;

    // function call
    printMyName();
    return 0;
}